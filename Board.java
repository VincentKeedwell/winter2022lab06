public class Board {
	private Die d1;
	private Die d2;
	private boolean[] closedTiles;
	
	public Board() {
		this.d1 = new Die();
		this.d2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString() {
		String toReturn = "";
		
		for(int i = 0; i < this.closedTiles.length; i++) {
			if (this.closedTiles[i] == false)
			{
				int tmp = i + 1;
				toReturn = toReturn + " " + tmp;
			}
			else
			{
				toReturn = toReturn + " X";
			}
		}
		
		return toReturn;
	}
	
	public boolean playATurn() {
		boolean result = false;
		
		this.d1.roll();
		this.d2.roll();
		
		System.out.println("You rolled: " + this.d1.getPips());
		System.out.println("And: " + this.d2.getPips());
		
		int totalNum = this.d1.getPips() + this.d2.getPips();
		System.out.println("Your total rolled is: " + totalNum);
		
		if (this.closedTiles[totalNum - 1] == false)
		{
			this.closedTiles[totalNum - 1] = true;
			System.out.println("Closing tile: " + totalNum);
		}
		else
		{
			System.out.println("The tile at position " + totalNum + " is already shut");
			result = true;
		}
		
		return result;
	}
}