import java.util.Random;

public class Die
{
	private int pips;
	private Random random;
	
	public Die() {
		this.pips = 1;
		this.random = new Random();
	}
	
	public int getPips() {
		return this.pips;
	}
	
	public int roll() {
		this.pips = random.nextInt(6) + 1;
		return this.pips;
	}
	
	public String toString() {
		return (this.pips+"");
	}
}