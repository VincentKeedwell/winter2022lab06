import java.util.Scanner;

public class ShutTheBox {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		boolean gameContinue = true;
		int player1Wins = 0;
		int player2Wins = 0;
		
		System.out.println("Welcome to Shut The Box, would you like to start a game? (y/n)");
		char gameBegin = reader.next().charAt(0);
		
		while (gameContinue) {
			//I created the board object and the gameOver boolean here like this they can reset every new game
			Board board = new Board();
			boolean gameOver = false;
			
			//checking if the player wants to play or not
			switch(gameBegin) {
				case 'y':
				case 'Y':
					do{
						System.out.println("Player 1's turn");
						System.out.println(board);
						if (board.playATurn())
						{
							System.out.println("Player 2 wins!");
							gameOver = true;
							player2Wins++;
						}
						else
						{
							System.out.println("");
							System.out.println("=============================");
							System.out.println("");
							
							System.out.println("Player 2's turn");
							System.out.println(board);
							if (board.playATurn())
							{
								System.out.println("Player 1 wins!");
								gameOver = true;
								player1Wins++;
							}
						}
						System.out.println("");
						System.out.println("=============================");
						System.out.println("");
					}while (gameOver == false);
					break;
				
				case 'n':
				case 'N':
					System.out.println("Thanks for Playing!");
					break;
				default:
					throw new IllegalArgumentException("Invalid Entry, only 'y' or 'n' are valid here.");
			}
			
			//checking if the player wants to play again
			System.out.println("Would you like to play again? (y/n)");
			char playAgain = reader.next().charAt(0);
			
			if (playAgain == 'n' || playAgain == 'N')
			{
				gameContinue = false;
				System.out.println("Thanks for Playing!");
				System.out.println("Player 1 Won " + player1Wins + " times!");
				System.out.println("Player 2 Won " + player2Wins + " times!");
			}
			else if (playAgain == 'y' || playAgain == 'Y')
			{
				System.out.println("");
				System.out.println("-----------------------------");
				System.out.println("Starting New Game!");
				System.out.println("-----------------------------");
			}
			else
			{
				throw new IllegalArgumentException("Invalid Entry, only 'y' or 'n' are valid here.");
			}
		}
	}
}